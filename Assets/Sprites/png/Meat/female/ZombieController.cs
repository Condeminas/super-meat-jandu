﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ZombieController : MonoBehaviour
{
    // Variables publicas para poder modifircar desde el inspector
    public float maxS = 11f;

    //Variables privadas
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float jump = 15f;
    // private bool right = false;
    private Animator anim;
    private AudioSource Audio;
    //public AudioSource jumpSound;
    public AudioClip Jump;
    public AudioClip Death;

    private bool flipped =  false;
    private bool jumping =  false;
    private bool walking =  false;
    private bool grounded = false;
    private bool muerto = false;
    private bool iddle = false;
    private object coll;
    private Vector3 origen;


    // Use this for initialization
    void Awake()
    {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();

        // Obtenemos el Animator Controller para poder modificar sus variables
        anim = GetComponent<Animator>();
        Audio = GetComponent<AudioSource>();
    }

    
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown("escape"))
        {
            Application.Quit();
        }
        if (!muerto)
        {
            //Miramos si hemos saltado
            if (!jumping && Input.GetKeyDown("space") && grounded)
            {
                Audio.clip = Jump;
                Audio.Play();
                grounded = false;
                jumping = true;
                rb2d.velocity = new Vector2(rb2d.velocity.x, jump);
                anim.SetBool("jumping", true);
                anim.SetBool("walking", false);
            }
            else if(jumping && Input.GetKeyDown("space")) {
               //Debug.Break();
                //anim.SetBool("iddle", false);
            }
            //jumping = false;


            //Miramos el input Horizontal
            move = Input.GetAxis("Horizontal");
            rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);

            //Miramos si nos estamos moviendo.
            // OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
            if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
            {
                if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
                {
                    flipped = !flipped;
                    this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
                }
                if (!jumping) {
                    anim.SetBool("walking", true); 
                    anim.SetBool("iddle", false); }
            }
            else
            {
                anim.SetBool("walking", false);
            }
        }
       
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "muerte")
        {           
            muerto = true;
            anim.SetBool("walking", false);
            anim.SetBool("dead", true);
            Audio.clip = Death;
            Audio.Play();
        }
        if (col.gameObject.tag == "Win")
        {
            //using UnityEngine.SceneManagement;
                //esto encima de todo (permite usar la libreria)
            SceneManager.LoadScene("GameOver");
        }
        grounded = true;
        jumping = false;
        anim.SetBool("jumping", false);
        
    }
    void Restart()
    {        
        this.transform.position = origen;
        anim.SetBool("dead", false);
        muerto = false;
        anim.Play("idle");
        anim.SetBool("iddle", true);
    }
    
}

