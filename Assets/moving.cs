﻿using UnityEngine;
using System.Collections;

public class moving : MonoBehaviour {

    private Rigidbody2D rb2D = null;
    private float move = 0f;
    //private bool right = false;

    public float maxS = 11f;

    public float jump = 150f;

    private bool jumping = false;

    private 

	// Use this for initialization
	void Awake () {
        rb2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        move = Input.GetAxis("Horizontal");
        rb2D.velocity = new Vector2(move * maxS, rb2D.velocity.y);

        if (Input.GetKeyDown("space") && !jumping)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, jump);
            jumping = true;
        }
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        jumping = false;
    }
}
